#!/usr/bin/python3

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from flask import (
    Flask,
    render_template,
    request,
    redirect,
    url_for,
    send_from_directory
)

import pyximport
pyximport.install()
import main
from log import make_pdf

app = Flask(__name__)


@app.route('/log')
def log():
    return render_template('log.html', **main.log())


@app.route('/generated.pdf')
def pdf():
    make_pdf(main.log()['data'])
    return send_from_directory('static', 'log.pdf')


@app.route('/')
def index():
    return redirect(url_for('drive'))


@app.route('/drive')
def drive():
    return render_template('drive.html', **main.drive())


@app.route('/startStop', methods=['GET', 'POST'])
def startStop():
    return ('', main.startStop())


@app.route('/night', methods=['GET', 'POST'])
def night():
    main.night()
    return ('', 204)

if __name__ == '__main__':
    # import threading
    # import webbrowser
    # url = 'http://127.0.0.1:5000'
    # threading.Timer(1.25, lambda: webbrowser.open_new(url)).start()
    app.run(debug=True)
