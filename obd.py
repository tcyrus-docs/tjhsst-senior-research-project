# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Testing Library for Python obd library

import time


class Debug(dict):
    def __init__(self) -> None:
        dict.__init__(self)


class Connection:
    def __init__(self) -> None:
        self.start = time.time()

    def is_connected(self):
        return True

    def query(self, command, force=False):
        end = time.time()
        return QueryResult(1.123456789 * (end - self.start))


class QueryResult:
    def __init__(self, val) -> None:
        self.value = val

    def is_null(self) -> bool:
        return False


class Commands():
    def __getitem__(self, key):
        if isinstance(key, int):
            return [0] * 31
        elif isinstance(key, str):
            return 0

commands = Commands()
debug = Debug()


def OBD():
    return Connection()
