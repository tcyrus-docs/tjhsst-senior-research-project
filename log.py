# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import os
import jinja2
from latex import build_pdf

# Change the default delimiters used by Jinja such that it won't pick up
# brackets attached to LaTeX macros.
latex_renderer = jinja2.Environment(
    block_start_string='%{',
    block_end_string='%}',
    variable_start_string='%{{',
    variable_end_string='%}}',
    comment_start_string='%%{',
    comment_end_string='%%}',
    loader=jinja2.FileSystemLoader(os.path.abspath('.'))
)


def make_pdf(log):
    latex = latex_renderer.get_template('templates/log.tex').render(log=log)
    build_pdf(latex).save_to('static/log.pdf')
