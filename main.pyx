# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import os, sys, csv, time
from decimal import Decimal

from openxc.vehicle import Vehicle
from openxc.measurements import Odometer
from openxc.interface import BluetoothVehicleInterface

from functions cimport *

raw, rawFile = None, None
cdef bint start = False
cdef str name = "INSERT NAME HERE"

vehicle = None

cdef str date
cdef float start_time, start_mileage, end_time, end_mileage
cdef float night_time, night_start_time
cdef bint night_log = False

cdef OpenXC_Connect():
    global vehicle
    try:
        vi = BluetoothVehicleInterface()
        vi.start()
        vehicle = Vehicle(vi)
        return True
    except DataSourceError:
        vehicle = None
        return False

cpdef dict log():
    global start, name
    if start: startStop()

    cdef list data = []
    cdef float total_time = 0.0, total_mileage = 0.0, night_time = 0.0
    cdef bint complete = False

    cdef str date
    cdef float time, night_t, mileage
    cdef list data_row

    if isFile("./raw.csv"):
        rawFile = open('raw.csv')
        raw = csv.DictReader(rawFile)

        for row in raw:
            date = row['date']
            time, night_t, mileage = float(row['total_time']), float(row['night_time']), float(row['mileage'])
            data_row = trip2data([time, night_t, mileage], total_time, total_mileage, night_time)
            total_time, total_mileage, night_time = data_row[0], data_row[1], data_row[2]
            data.append([date, time2str(int(total_time)), "%.1f" % total_mileage, time2str(int(night_time))])

        rawFile.close()
        complete = ((total_time // 360) >= 45 and (night_time // 360) >= 15)
    return {'data': data, 'name': name, 'complete': complete}

cpdef dict drive():
    global connection
    if not connection.is_connected(): connection = obd.OBD()
    return {'obdStatus': connection.is_connected()}

cpdef int startStop():
    global start, start_time, end_time
    global night_log, night_time, night_start_time
    global end_mileage, start_mileage
    global raw, rawFile
    global date, connection

    if not vehicle:
        if not OpenXC_Connect():
            return 500

    if not start:
        start_time, start_mileage, end_time, end_mileage = 0.0, 0.0, 0.0, 0.0
        night_time, night_start_time = 0.0, 0.0

        if night_log: nighttime()
        if not isFile("./raw.csv"):
            rawFile = open('raw.csv', 'w')
            rawFile.write("date,total_time,mileage,night_time\n")
        else:
            rawFile = open('raw.csv', 'a')

        date = time.strftime("%-m/%-d/%Y")
        r = vehicle.get(Odometer)

        if r:
            print("Start Mileage is NULL")
            return 500

        start_mileage, start_time = r.value, r.created_at
    else:
        r = vehicle.get(Odometer)

        if r:
            print("End Mileage is NULL")
            return 500

        end_mileage, end_time = r.value, r.created_at
        if night_log: nighttime()
        rawFile.write('{},{!s},{!s},{!s}\n'.format(date, end_time - start_time, mRound(end_mileage - start_mileage), night_time))
        rawFile.close()
    start = (not start)
    return 204

cpdef night():
    global start, night_log
    if start: nighttime()
    night_log = (not night_log)

cpdef nighttime():
    global night_time, night_start_time
    if night_start_time == 0:
        night_start_time = time.time()
    else:
        night_time += time.time() - night_start_time
        night_start_time = 0
