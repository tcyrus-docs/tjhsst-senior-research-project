# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

cdef float mRound(float)

cdef list trip2data(list, float, float, float)

cdef str time2str(int)

cdef bint isFile(str)
