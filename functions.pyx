# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from libc.stdlib cimport malloc
from libc.stdio cimport sprintf
from libc.math cimport round
from posix.stat cimport struct_stat, stat

cdef float mRound(float mileage):
    return round(mileage * 10.0) / 10.0

cdef list trip2data(list trip, float total_time, float total_mileage, float night_time):
    cdef float time = trip[0], night_t = trip[1], mileage = trip[2]
    total_time += time
    night_time += night_t
    total_mileage += mileage
    return [total_time, total_mileage, night_time]

cdef str time2str(int t):
    cdef int m = t / 60, s = t % 60, h = m / 60
    m %= 60
    cdef char* buffer = <char*> malloc(sizeof(char) * 15);
    sprintf(buffer, "%d:%02d:%02d", h, m, s)
    return buffer.decode('UTF-8')

cdef bint isFile(str path):
    cdef bytes byte_string = path.encode('UTF-8')
    cdef char* filePath = byte_string
    cdef struct_stat buffer
    return (stat(filePath, &buffer) == 0)
